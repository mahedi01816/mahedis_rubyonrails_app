== README

This is a simple app like twitter, where one user can follow other users and 
at the same time followed by other users.

Ruby version
2.0

Rails version
4.2.2

Development Database
sqlite3

If you want to resize image then you have to install ImageMagick in your development environment.